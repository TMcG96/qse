from flask import Flask, request, render_template, jsonify
from flask_cors import CORS
import mysql.connector
import requests
import json

app = Flask(__name__)
cors = CORS(app)

# searchpages
@app.route('/search')
def search():

    pageNum = int(request.args.get('pageNum'))
    search = request.args.get('q')

    pload = {'q': search}
    r = requests.get("http://35.187.68.167:5001/",params=pload)
    r_dict = r.json()
    records = r_dict['records']
    adds = r_dict['adds']

    length = len(records)
    # return render_template("index.html", test= search)
    return render_template("search.html", test= search, testarray = records[(0+10*pageNum):(9+10*pageNum)], pageNum=pageNum, length=length, adds = adds)

@app.route('/')
def main():

    return render_template("index.html")

if __name__ == '__main__':
    app.run(host='0.0.0.0', port = '5000')
